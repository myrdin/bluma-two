# Bluma-two

Theme for bludit CMS made with bulma css framework.

Inspired by [Bluma theme](https://themes.bludit.com/theme/bluma) and [Bulma hero template](https://dansup.github.io/bulma-templates/templates/hero.html).

Download the theme and copy the theme folder into the folder `bl-themes` then you can activate it in the admin panel.


