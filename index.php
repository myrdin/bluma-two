<!DOCTYPE html>
<html>
  <!-- Header -->
  <head>
    <?php include(THEME_DIR_PHP.'head.php'); ?>
  </head>
  <body>
    <!-- Load Bludit plugins: site body begin -->
    <?php Theme::plugins('siteBodyBegin'); ?>
    <div class="wrapper">
      <!-- Load navbar or hero -->
      <?php
      if ($WHERE_AM_I == 'page') {
      include(THEME_DIR_PHP.'navbar.php');
      } else {
      include(THEME_DIR_PHP.'hero.php');
      }
      ?>
      <div class="columns articles">
        <div class="column is-three-quarters">
          <!-- Load homepage or articles -->
          <?php
          if ($WHERE_AM_I == 'home') {
          include(THEME_DIR_PHP.'home.php');
          } else {
          include(THEME_DIR_PHP.'page.php');
          }
          ?>
        </div>
        <!-- Sidebar -->
        <div class="column">
          <div class="sidebar">
            <?php Theme::plugins('siteSidebar') ?>
          </div>
        </div>
      </div>
    </div>
    <!-- Footer -->
    <?php include(THEME_DIR_PHP.'footer.php'); ?>
    <!-- Load Bludit plugins: site body end -->
    <?php Theme::plugins('siteBodyEnd'); ?>
  </body>
</html>