<!-- Post -->
<div class="content">
	<!--  Load Bludit plugins: page begin -->
	<?php Theme::plugins('pageBegin'); ?>
	<div class="columns">
		<div class="column is-half is-offset-one-quarter">
			<!-- Cover image -->
			<?php if ($page->coverImage()) : ?>
			<img alt="Cover Image" src="<?php echo $page->coverImage(); ?>"/>
			<?php endif ?>
			<!-- Title -->
			<a class="text-dark" href="<?php echo $page->permalink(); ?>">
				<h1 class="title"><?php echo $page->title(); ?></h1>
			</a>
			<?php if (!$page->isStatic() && !$Url->notFound()) : ?>
			<!-- Creation date -->
			<h6><?php echo $page->date(); ?> - <?php echo $Language->get('Reading time') . ': ' . $page->readingTime() ?></h6>
			<?php endif ?>
			<!-- Full content -->
			<?php echo $page->content(); ?>
		</div>
	</div>
	<!-- Load Bludit plugins: page end -->
	<?php Theme::plugins('pageEnd'); ?>
</div>