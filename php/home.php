<!--  Load Bludit plugins: page begin -->
<?php Theme::plugins('pageBegin'); ?>
<!-- Cards -->
<div class="columns posts">
  <?php foreach ($content as $page) : ?>
  <div class="column is-4">
    <div class="card is-shady">
      <div class="card-image has-text-centered">
        <?php if ($page->coverImage()) : ?>
        <img alt="Cover Image" src="<?php echo $page->coverImage(); ?>"/>
        <?php endif ?>
      </div>
      <div class="card-content">
        <div class="content">
          <a href="<?php echo $page->permalink(); ?>">
            <h4 class="title"><?php echo $page->title(); ?></h4>
          </a>
          <h6><?php echo $page->date(); ?> - <?php echo $Language->get('Reading time') . ': ' . $page->readingTime(); ?></h6>
          <a href="<?php echo $page->permalink(); ?>" class='button is-outlined'><?php echo $Language->get('Read more'); ?></a>
        </div>
      </div>
    </div>
  </div>
  <?php endforeach ?>
</div>
<!-- Load Bludit plugins: page end -->
<?php Theme::plugins('pageEnd'); ?>
<!-- Pagination -->
<?php if (Paginator::amountOfPages()>1) : ?>
<nav class="pagination" role="navigation" aria-label="pagination">
  <?php
  // Previous button
  if (!Paginator::showPrev()) {
  echo '<a class="button is-primary disabled" disabled>« ' . $Language->get('Previous') . '</a>';
  } else {
  echo '<a class="button is-primary" href="' . Paginator::prevPageUrl() .'">« ' . $Language->get('Previous') . '</a>';
  }
  // Next button
  if (!Paginator::showNext()) {
  echo '<a class="button is-primary disabled" disabled>' . $Language->get('Next') . ' »</a>';
  } else {
  echo '<a class="button is-primary" href="' . Paginator::nextPageUrl() .'">' . $Language->get('Next') . ' »</a>';
  }
  ?>
</nav>
<?php endif ?>