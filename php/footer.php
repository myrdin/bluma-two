<footer class="footer">
    <div class="container">
        <div class="content has-text-centered">
            <!-- Social Networks -->
            <?php if ($site->github()) : ?>
            <a href="<?php echo $site->github() ?>" target="_blank">
                <i class="fa fa-github"></i>
            </a>
            <?php endif ?>
            <?php if ($site->twitter()) : ?>
            <a href="<?php echo $site->twitter() ?>" target="_blank">
                <i class="fa fa-twitter"></i>
            </a>
            <?php endif ?>
            <?php if ($site->facebook()) : ?>
            <a href="<?php echo $site->facebook() ?>" target="_blank">
                <i class="fa fa-facebook-f"></i>
            </a>
            <?php endif ?>
            <?php if ($site->googleplus()) : ?>
            <a href="<?php echo $site->googleplus() ?>" target="_blank">
                <i class="fa fa-google-plus"></i>
            </a>
            <?php endif ?>
            <?php if ($site->codepen()) : ?>
            <a href="<?php echo $site->codepen() ?>" target="_blank">
                <i class="fa fa-codepen"></i>
            </a>
            <?php endif ?>
            <?php if ($site->linkedin()) : ?>
            <a href="<?php echo $site->linkedin() ?>" target="_blank">
                <i class="fa fa-linkedin-in"></i>
            </a>
            <?php endif ?>
            <!-- Footer infos -->
            <p><?php echo $site->footer(); ?></p>
        </div>
    </div>
</footer>