<!-- Hero for homepage -->
<section class="hero is-primary is-medium is-bold">
  <?php include(THEME_DIR_PHP.'navbar.php'); ?>
  <div class="hero-body">
    <div class="container has-text-centered">
      <a id="title" class="is-size-1" href="<?php echo $site->url() ?>">
        <?php echo $site->title() ?>
      </a>
      <p class="is-size-3">
        <?php echo $site->slogan() ?>
      </p>
    </div>
  </div>
</section>