<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Dynamic title tag -->
<?php echo Theme::headTitle(); ?>

<!-- Dynamic description tag -->
<?php echo Theme::headDescription(); ?>

<!-- Favicon -->
<?php echo Theme::favicon('img/favicon.png'); ?>

<!-- CSS: Bulma -->
<?php echo Theme::css('css/bulma.min.css'); ?>

<!-- CSS: Styles for this theme -->
<?php echo Theme::css('css/style.css'); ?>

<!-- JS -->
<?php echo Theme::js('js/main.js'); ?>

<!-- Icons: Font-awesome -->
<?php echo Theme::fontAwesome(); ?>
