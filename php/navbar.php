<div class="hero-head">
  <nav class="navbar is-primary">
    <div class="container">
      <div class="navbar-brand">
        <a class="navbar-item" href="<?php echo $site->url() ?>">
          <img class="logo" src="<?php echo HTML_PATH_ADMIN_THEME ?>img/logo.svg" />
        </a>
        <span class="navbar-burger burger" data-target="navbarMenu">
          <span></span>
          <span></span>
          <span></span>
        </span>
      </div>
      <div id="navbarMenu" class="navbar-menu">
        <div class="navbar-end">
          <a class="navbar-item" href="<?php echo $site->url() ?>">Home</a></li>
          <!-- Static pages -->
          <?php foreach ($staticContent as $staticPage) : ?>
          <a class="navbar-item" href="<?php echo $staticPage->permalink() ?>"><?php echo $staticPage->title() ?></a>
          <?php endforeach ?>
          <form class="navbar-item" role="search" action="<?php echo $Site->url() . 'search.php';?>" target="_blank">
            <input class='input' type="text" name="q" placeholder="Search...">
          </form>
        </div>
      </div>
    </nav>
  </div>